class Move {
  constructor(value, move) {
    this.value = value;
    this.move = move;
  }
}

let gameEnded = false;
let squares = document.getElementsByClassName("square");
let gameState;

function init() {
  clearBoard();
  gameEnded = false;
  gameState = [0,0,0,0,0,0,0,0,0,false];
  document.getElementById("gameStatus").innerHTML = "";
  if (document.getElementById("computerstarts").checked) {
    gameLoop();
  } else {
    showHints();
  }
  drawGameState();

  for (var i = 0; i < squares.length; i++) {
    squares[i].addEventListener('click', event => {
      var targetElement = event.target || event.srcElement;
      let clickedItemID = targetElement.getAttribute("value");
      if (gameState[clickedItemID] == 0 && !gameEnded) {
        gameState[clickedItemID] = gameState[9] ? 1:-1;
        gameState[9] = !gameState[9];
        gameLoop();
      }
    })

  }
}

function drawGameState() {
  for (var i = 0; i < squares.length; i++) {
    squares[i].setAttribute("value", i);
    if (gameState[i] == 1) {
      squares[i].setAttribute("class", "square x");
    }
    if (gameState[i] == -1) {
      squares[i].setAttribute("class", "square o");
    }

  }
}

function clearBoard() {
  for (var i = 0; i < squares.length; i++) {
    squares[i].setAttribute("value", i);
    squares[i].setAttribute("class", "square");
  }
}

function showHints() {
  let copyGameState = [];
  for (var i = 0; i < gameState.length; i++) {
    copyGameState.push(gameState[i]);
  }

  let nodes = [];
  for (var i = 0; i < 9; i++) {
    if (copyGameState[i] == 0) {
      nodes.push(i);
    }
  }
  for (var i = 0; i < nodes.length; i++) {
    let res = 100;
    if (document.getElementById("hint").checked) {
      copyGameState[nodes[i]] = copyGameState[9] ? 1 : -1;
      copyGameState[9] = !copyGameState[9];
      res = minMax(copyGameState).value;
      copyGameState[9] = !copyGameState[9];
      copyGameState[nodes[i]] = 0;
    }
    if (res == 0) {
      squares[nodes[i]].setAttribute("class", "square draw");
    }

    if (res == (copyGameState[9]?1:-1)) {
      squares[nodes[i]].setAttribute("class", "square win");
    }

    if (res == (copyGameState[9]?-1:1)) {
      squares[nodes[i]].setAttribute("class", "square lost");
    }

    if (res == 100) {
      squares[nodes[i]].setAttribute("class", "square");
    }
  }
}

function areEqual(){
   var len = arguments.length;
   for (var i = 1; i< len; i++){
      if (arguments[i] === null || arguments[i] !== arguments[i-1])
         return false;
   }
   return true;
}

function whoIsTheWinner(gameState) {
  for (var i = 0; i < 3; i++) {
    if (areEqual(gameState[3*i+0], gameState[3*i+1],  gameState[3*i+2]) && gameState[3*i+0] != 0) {
      return gameState[3*i+0];
    }

    if (areEqual(gameState[0+i], gameState[3+i],  gameState[6+i]) && gameState[0+i] != 0) {
      return gameState[0+i];
    }
  }

  if ((areEqual(gameState[0], gameState[4],  gameState[8]) ||
      areEqual(gameState[2], gameState[4],  gameState[6]))  && gameState[4] != 0) {
        return gameState[4];
  }

  let draw = true;
  for (var i = 0; i < 9; i++) {
    if (gameState[i] == 0) {
      draw = false;
    }
  }
  return draw ? 0:100;
}

function minMax(gameState) {
  let result = whoIsTheWinner(gameState);
  if (result != 100) {
    return new Move(result);
  }

  let nodes = [];
  for (var i = 0; i < 9; i++) {
    if (gameState[i] == 0) {
      nodes.push(i);
    }
  }

  let bestMoveArray = [];
  let best;
  if (gameState[9]) {
    //Maximaze
    best = -999;
    for (var i = 0; i < nodes.length; i++) {
      gameState[nodes[i]] = 1;
      gameState[9] = !gameState[9];
      let res = minMax(gameState).value;
      gameState[nodes[i]] = 0;
      gameState[9] = !gameState[9];
      if (res > best) {
        best = res;
        bestMoveArray = [];
        bestMoveArray.push(new Move(res,nodes[i]));
      } else if (res == best) {
        bestMoveArray.push(new Move(res,nodes[i]));
      }
    }
  } else {
    //Minimaze
    best = 999;
    for (var i = 0; i < nodes.length; i++) {
      gameState[nodes[i]] = -1;
      gameState[9] = !gameState[9];
      let res = minMax(gameState).value;
      gameState[nodes[i]] = 0;
      gameState[9] = !gameState[9];
      if (res < best) {
        best = res;
        bestMoveArray = [];
        bestMoveArray.push(new Move(res,nodes[i]));
      } else if (res == best) {
        bestMoveArray.push(new Move(res,nodes[i]));
      }
    }
  }
  return bestMoveArray[Math.floor(Math.random() * bestMoveArray.length)];
}

function gameover(result, userMoved) {
  gameEnded = true;
  if (result == 0) {
    document.getElementById("gameStatus").innerHTML = "Draw";
  } else {
    if (userMoved) {
      document.getElementById("gameStatus").innerHTML = "Win";
    } else {
      document.getElementById("gameStatus").innerHTML = ":'(";
    }
  }
  document.getElementById("gameStatus").setAttribute("class", "visible");

}

function gameLoop() {
  let result = whoIsTheWinner(gameState);
  if (result == 100) {
    let bm = minMax(gameState);
    console.log(bm.value);
    gameState[bm.move] = gameState[9] ? 1:-1;
  } else {
    gameover(result, true);
  }
  result = whoIsTheWinner(gameState);
  if (result != 100 && !gameEnded) {
    gameover(result, false);
  }
  gameState[9] = !gameState[9];
  showHints();
  drawGameState();
}
